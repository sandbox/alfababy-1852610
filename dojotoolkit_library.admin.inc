<?php
/**
 * @file
 * Functionality for dojotoolkit library administration
 */

/**
 * Admin settings form.
 */
function dojotoolkit_library_admin_settings($form, &$form_state) {
  $form['dojotoolkit_library_version'] = array(
    '#type' => 'select',
    '#title' => t('Version'),
    '#default_value' => variable_get('dojotoolkit_library_version', '1.8.1'),
    '#options' => drupal_map_assoc(_dojotoolkit_library_provide_version()),
  );

  $form['dojotoolkit_library_package_base_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Dojotoolkit package base path'),
    '#default_value' => variable_get('dojotoolkit_library_package_base_path', DEFAULT_PACKAGE_BASE_URL),
    '#states' => array(
      'invisible' => array(
        'select[name="dojotoolkit_library_version"]' => array(array("value" => "1.6.0"), array("value" => "1.6.1")),
      ),
    ),
  );

  return system_settings_form($form);
}
