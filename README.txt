This is a sandbox project, which contains experimental code for developer 
use only.

Visit the Version control tab for full directions.
Dojo toolkit (http://dojotoolkit.org) is a Javascript platform.

This is a library API module to support loading Dojo from a CDN. 
And auto loading package that you define the path (Dojo 1.7+).

How to use it:
1. Set which version do you want to load it. 
(admin - config - dojotoolkit library).

2. Use drupal standard way to load it: 
<?php drupal_add_library('dojotoolkit_library', 'dojotoolkit'); ?>


-----------------------------------------------------------------------------

#Sample
function dojotoolkit_library_init(){
  drupal_add_library('dojotoolkit_library', 'dojotoolkit');
  
  //Dojo 1.6
  $js = <<<EOT
    require(["dojo/query", "dojo/domReady!"], function(query) {
      var input = query(".claro");
      console.log(input);
    });
EOT;

  //Dojo 1.7+
  $js = <<<EOT
    dojo.ready(function(){
      var input = dojo.query(".claro");
      console.dir(input);
    });
EOT;

  drupal_add_js($js, 'inline');
}
